﻿is_centaur = {
	var:race_id ?= 1
    # has_discrimination_trait = centaur_race_heritage
}

is_dwarven = {
	var:race_id ?= 2
    # has_discrimination_trait = dwarven_race_heritage
}

is_elven = {
	var:race_id ?= 3
    # has_discrimination_trait = elven_race_heritage
}

is_gnollish = {
	var:race_id ?= 4
    # has_discrimination_trait = gnollish_race_heritage
}

is_gnomish = {
	var:race_id ?= 5
    # has_discrimination_trait = gnomish_race_heritage
}

is_goblin = {
	var:race_id ?= 6
    # has_discrimination_trait = goblin_race_heritage
}

is_halfling = {
	var:race_id ?= 7
    # has_discrimination_trait = halfling_race_heritage
}

is_half_elven = {
	var:race_id ?= 8
    # has_discrimination_trait = half_elven_race_heritage
}

is_half_orcish = {
	var:race_id ?= 9
    # has_discrimination_trait = half_orcish_race_heritage
}

is_harimari = {
	var:race_id ?= 10
    # has_discrimination_trait = harimari_race_heritage
}

is_harpy = {
	var:race_id ?= 11
    # has_discrimination_trait = harpy_race_heritage
}

is_hobgoblin = {
	var:race_id ?= 12
    # has_discrimination_trait = hobgoblin_race_heritage
}

is_human = {
	custom_description = {
		text = is_human_race_tt
		var:race_id ?= 13
	}
}

is_kobold = {
	var:race_id ?= 14
    # has_discrimination_trait = kobold_race_heritage
}

is_lizardman = {
	var:race_id ?= 15
    # has_discrimination_trait = lizardman_race_heritage
}

is_mechanim = {
	var:race_id ?= 16
    # has_discrimination_trait = mechanim_race_heritage
}

is_ogre = {
	var:race_id ?= 17
    # has_discrimination_trait = ogre_race_heritage
}

is_orcish = {
	var:race_id ?= 18
    # has_discrimination_trait = orcish_race_heritage
}

is_ruinborn = {
	custom_description = {
		text = is_ruinborn_race_tt
		var:race_id ?= 19
	}
}

is_troll = {
    # has_discrimination_trait = troll_race_heritage
	var:race_id ?= 20
}

is_degenerated_elf = {
	is_ruinborn = yes
	has_discrimination_trait = degenerated_elf
}

is_same_race_as_any_primary_culture = {
	custom_description = {
		text = is_same_race_as_any_primary_culture_tt
		# List every race here
		OR = {
			AND = {
				is_centaur = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_centaur = yes
					}
				}
			}
			AND = {
				is_dwarven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_dwarven = yes
					}
				}
			}
			AND = {
				is_elven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_elven = yes
					}
				}
			}
			AND = {
				is_gnollish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_gnollish = yes
					}
				}
			}
			AND = {
				is_gnomish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_gnomish = yes
					}
				}
			}
			AND = {
				is_goblin = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_goblin = yes
					}
				}
			}
			AND = {
				is_halfling = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_halfling = yes
					}
				}
			}
			AND = {
				is_half_elven = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_half_elven = yes
					}
				}
			}
			AND = {
				is_half_orcish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_half_orcish = yes
					}
				}
			}
			AND = {
				is_harimari = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_harimari = yes
					}
				}
			}
			AND = {
				is_harpy = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_harpy = yes
					}
				}
			}
			AND = {
				is_hobgoblin = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_hobgoblin = yes
					}
				}
			}
			AND = {
				is_human = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_human = yes
					}
				}
			}
			AND = {
				is_kobold = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_kobold = yes
					}
				}
			}
			AND = {
				is_ogre = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_ogre = yes
					}
				}
			}
			AND = {
				is_orcish = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_orcish = yes
					}
				}
			}
			AND = {
				is_ruinborn = yes
				# Special stuff for Ruinborn so we don't need to make race for each of them
				shares_heritage_trait_with_any_primary_culture = $COUNTRY$
			}
			AND = {
				is_lizardman = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_lizardman = yes
					}
				}
			}
			AND = {
				is_troll = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_troll = yes
					}
				}
			}
			AND = {
				is_troll = yes
				$COUNTRY$ = {
					any_primary_culture = {
						is_troll = yes
					}
				}
			}
		}
	}
}

is_goblinoid_group = {
	custom_description = {
		text = is_goblinoid_group_tt
		OR = {
			is_goblin = yes
			is_hobgoblin = yes
		}
	}
}

is_ruinborn_group = {
	custom_description = {
		text = is_ruinborn_group_tt
		is_ruinborn = yes
	}
}

is_giantkin_group = {
	custom_description = {
		text = is_giantkin_group_tt
		OR = {
			is_troll = yes
			is_ogre = yes
		}
	}
}

is_monstrous_culture = {
	custom_description = {
		text = is_monstrous_race_tt
		OR = {
			is_centaur = yes
			is_kobold = yes
			is_gnollish = yes
			is_goblin = yes
			is_harpy = yes
			is_hobgoblin = yes
			is_lizardman = yes
			is_ogre = yes
			is_orcish = yes
			is_half_orcish = yes
			is_troll = yes
			is_degenerated_elf = yes
		}
	}
}

is_non_monstrous_culture = {
	custom_description = {
		text = is_non_monstrous_race_tt
		NOT = { is_monstrous_culture = yes }
	}
}

pop_race_accepted = {
	custom_description = {
		text = race_is_accepted_by_racial_laws_tt
		culture = {
			cultural_race_accepted = {
				COUNTRY = prev.$COUNTRY$
			}
		}
	}
}

cultural_race_accepted = {
	custom_description = {
		text = race_is_accepted_by_racial_laws_tt
		OR = {
			AND = {
				$COUNTRY$ = { has_law = law_type:law_same_race_only }
				is_same_race_as_any_primary_culture = { # Always accept your own race
					COUNTRY = $COUNTRY$
				}
			}
			
			# Monstrous vs non-monstrous
			AND = {
				$COUNTRY$ = { has_law = law_type:law_monstrous_only }
				is_monstrous_culture = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_non_monstrous_only }
				is_non_monstrous_culture = yes
			}
			
			#Same group
			AND = {
				$COUNTRY$ = { has_law = law_type:law_giantkin_group_only }
				is_giantkin_group = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_ruinborn_group_only }
				is_ruinborn_group = yes
			}
			AND = {
				$COUNTRY$ = { has_law = law_type:law_goblinoid_group_only }
				is_goblinoid_group = yes
			}
			
			$COUNTRY$ = { has_law = law_type:law_all_races_allowed }
		}
	}
}

will_be_accepted_race = {
	OR = {
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_same_race_only }
			is_same_race_as_any_primary_culture = { # Always accept your own race
				COUNTRY = $COUNTRY$
			}
		}
		
		# Monstrous vs non-monstrous
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_monstrous_only }
			is_monstrous_culture = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_non_monstrous_only }
			is_non_monstrous_culture = yes
		}
		
		#Same group
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_giantkin_group_only }
			is_giantkin_group = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_ruinborn_group_only }
			is_ruinborn_group = yes
		}
		AND = {
			$COUNTRY$ = { is_enacting_law = law_type:law_goblinoid_group_only }
			is_goblinoid_group = yes
		}
		
		$COUNTRY$ = { is_enacting_law = law_type:law_all_races_allowed }
	}
}

#Checks if pop is accepted by CULTURAL laws
is_accepted_culture = {
    OR = {
        AND = {
            $COUNTRY$ = { has_law = law_type:law_ethnostate }
            is_primary_culture_of = $COUNTRY$
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_national_supremacy }
            AND = {
				shares_heritage_trait_with_any_primary_culture = $COUNTRY$
				shares_non_heritage_trait_with_any_primary_culture = $COUNTRY$
			}
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_racial_segregation }
            shares_heritage_trait_with_any_primary_culture = $COUNTRY$
        }
        AND = {
            $COUNTRY$ = { has_law = law_type:law_cultural_exclusion }
            shares_trait_with_any_primary_culture = $COUNTRY$
        }
        $COUNTRY$ = { has_law = law_type:law_multicultural }
    }
}

culture_has_no_magical_affinity = {
	is_mechanim = yes
}

culture_has_good_magical_affinity = {
	OR = {
		is_elven = yes
		is_gnomish = yes
	}
}

culture_has_low_magical_affinity = {
	OR = {
		is_dwarven = yes
		is_goblin = yes
		is_orcish = yes
	}
}

pop_race_good_magical_affinity = {
	this.culture = {
		culture_has_good_magical_affinity = yes
	}
}

pop_race_low_magical_affinity = {
	this.culture = {
		culture_has_low_magical_affinity = yes
	}
}

pop_race_no_magical_affinity = {
	this.culture = {
		culture_has_no_magical_affinity = yes
	}
}