﻿ruinborn_mteibhar = {
	template = "moon_elf"

	
	skin_color = {
		10 = { 0.3 0.15 0.7 0.28 }
	}
	eye_color = {
        # Green
        80 = { 0.33 0.5 0.67 0.8 }
		# Brown
		10 = { 0.05 0.7 0.35 1.0 }
	}
	hair_color = {
        # # Brown
        40 = { 0.65 0.45 0.9 1.0 }
        # # Red
        40 = { 0.85 0.0 1.0 0.5 }
        # # Black
        10 = { 0.0 0.9 0.5 0.99 }
	}

	hairstyles = {
		10 = { name = native_american_hairstyles 		range = { 0.0 1.0 } }
	}
}

	