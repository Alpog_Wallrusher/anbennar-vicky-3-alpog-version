﻿# Strategic Regions in Western Europe

region_bahar = {
	graphical_culture = "arabic"
	capital_province = "x4C5040" #Aqatbar
	map_color = { 100 100 200 }
	states = { STATE_REUYEL STATE_MEDBAHAR STATE_MEGAIROUS STATE_AQATBAHAR STATE_BAHAR_PROPER STATE_CRATHANOR STATE_OVDAL_TUNGR STATE_OURDIA }
}

region_harpy_hills = {
	graphical_culture = "arabic"
	capital_province = "x5e4800" #Harpylen
	map_color = { 200 169 168 }
	states = { STATE_KUZARAM STATE_GARLAS_KEL STATE_HARPYLEN STATE_EAST_HARPY_HILLS STATE_FIRANYALEN }
}

region_bulwar_proper = {
	graphical_culture = "arabic"
	capital_province = "xd55240" #Bulwar
	map_color = { 205 233 232 }
	states = { STATE_BRASAN STATE_SAD_SUR STATE_LOWER_BURANUN STATE_LOWER_SURAN STATE_BULWAR STATE_KUMARKAND STATE_DROLAS STATE_ANNAIL }
}

region_far_bulwar = {
	graphical_culture = "arabic"
	capital_province = "xd98200" #Jaddanzar
	map_color = { 241 135 50 }
	states = { STATE_WEST_NAZA STATE_EAST_NAZA STATE_JADDANZAR STATE_AVAMEZAN STATE_UPPER_SURAN STATE_AZKA_SUR }
}

region_far_salahad = {
	graphical_culture = "arabic"
	capital_province = "x38cb14" #Ebbusubtu
	map_color = { 253 249 80 }
	states = { STATE_ARDU STATE_KERUHAR STATE_FAR_EAST_SALAHAD STATE_EBBUSUBTU STATE_MULEN STATE_ELAYENNA }
}