﻿## Urban Centers
pm_department_stores = {
	texture = "gfx/interface/icons/production_method_icons/arcades.dds"
	unlocking_technologies = {
		department_stores
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_glass_add = 2
			goods_input_steel_add = 2
			goods_input_electricity_add = 1
			goods_input_artificery_doodads_add = 2
			goods_output_services_add = 40
		}

		level_scaled = {
			building_employment_clerks_add = 3500
			building_employment_shopkeepers_add = 1500
		}

	}

	required_input_goods = electricity
}

pm_mood_enhancing_starlight = {
	texture = "gfx/interface/icons/production_method_icons/electric_streetlights.dds"

	city_lights_color_index = 2	# [DO SOMETHING COOL HERE]

	unlocking_technologies = {
		electroarcanism_theory
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_artificery_doodads_add = 5
			goods_output_services_add = 18
		}

		level_scaled = {
			building_employment_laborers_add = 150
			building_employment_machinists_add = 50
			building_employment_engineers_add = 50
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 3
		}
	}
}

pm_vendorless_stalls = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		early_mechanim		
	}

	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 200
    	#}
    #}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 5	
		}

		level_scaled = {
			building_employment_laborers_add = -1500
		}
	}
}

pm_automata_shopfronts = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		advanced_mechanim			
	}

	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 400
    	#}
    #}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 10	
		}

		level_scaled = {
			building_employment_clerks_add = -2500
			building_employment_laborers_add = -500
			building_employment_machinists_add = -400
		}
	}
}

## Art Academies
pm_runic_art = {
	texture = "gfx/interface/icons/production_method_icons/realist_art.dds"

	unlocking_technologies = {
		realism
	}

	building_modifiers = {

		workforce_scaled = { 
			goods_input_iron_add = 6
			goods_input_tools_add = 5
			goods_input_dye_add = 4
			goods_output_fine_art_add = 6
		}
	}
}

pm_viewcatcher_art = {
	texture = "gfx/interface/icons/production_method_icons/photographic_art.dds"

	unlocking_technologies = {
		camera
	}

	building_modifiers = {

		workforce_scaled = { # 630 profit
			goods_input_paper_add = 15 # 450
			goods_input_tools_add = 8 # 320

			goods_output_fine_art_add = 7 # 1400
		}
	}
}

## Power Plants
pm_damestear_reactor = {
	texture = "gfx/interface/icons/production_method_icons/oil_fired_plant.dds"

	unlocking_technologies = {
		the_fifth_fundamental_force
	}

	state_modifiers = {
		workforce_scaled = {
			state_pollution_generation_add = 75
		}
	}

	building_modifiers = {
		workforce_scaled = {
			goods_input_engines_add = 25
			goods_input_damestear_add = 50
			goods_input_porcelain_add = 10
			goods_output_electricity_add = 225
		}

		level_scaled = {
			building_employment_laborers_add = 1000
			building_employment_machinists_add = 1500
			building_employment_engineers_add = 2400
		}
	}
}

pm_automata_laborers_power_plants = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		insyaan_analytical_engine		
	}

	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 240
    	#}
    #}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 6	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
		}
	}
}

pm_automata_machinists_power_plants = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	
	#state_modifiers = {
    	#workforce_scaled = {
    	#	state_pops_assembled_add = 400
    	#}
    #}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 10	
		}

		level_scaled = {
			building_employment_laborers_add = -3000
			building_employment_machinists_add = -1750
		}
	}
}