company_strongbellow_shipyard = {
	icon = "gfx/interface/icons/company_icons/basic_shipyards.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_harbor_shipbuilding.dds"

	flavored_company = yes

	building_types = {
		building_shipyards
		building_military_shipyards
		building_motor_industry
	}

	potential = {
		has_interest_marker_in_region = region_bahar
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_OVDAL_TUNGR
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_OVDAL_TUNGR
			any_scope_building = {
				is_building_type = building_motor_industry
				level >=4
			}
		}
	}

	prosperity_modifier = {
		country_convoys_capacity_mult = 0.1
		building_coal_mine_throughput_add = 0.05
	}

	ai_weight = {
		value = 3
	}
}

company_massive_shaft = { #Massive Shaft Deep Drilled Fracking
	icon = "gfx/interface/icons/company_icons/basic_mineral_mining.dds"
	background = "gfx/interface/icons/company_icons/company_backgrounds/comp_illu_mining.dds"

	flavored_company = yes

	building_types = {
		building_iron_mine
		building_explosives_factory
	}

	potential = {
		has_interest_marker_in_region = region_mountainheart
	}

	attainable = {
		hidden_trigger = {
			any_scope_state = {
				state_region = s:STATE_DEAD_CAVERNS
			}
		}
	}

	possible = {
		any_scope_state = {
			state_region = s:STATE_DEAD_CAVERNS
			any_scope_building = {
				is_building_type = building_explosives_factory
				level >= 2
			}
		}
	}

	prosperity_modifier = {
		state_pollution_reduction_health_mult = -0.1
		building_group_bg_mining_throughput_add = 0.1
	}

	ai_weight = {
		value = 3
	}
}