﻿BUILDINGS={
	s:STATE_STEEL_BAY = {
		region_state:B05 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:B05"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_tooling_workshops"
						country="c:B05"
						levels=1
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_steel" "pm_automation_disabled" }
			}
			create_building={
				building="building_steel_mills"
				add_ownership={
					building={
						type="building_steel_mills"
						country="c:B05"
						levels=1
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_blister_steel_process" "pm_automation_disabled" }
			}
			create_building={
				building="building_arms_industry"
				add_ownership={
					building={
						type="building_arms_industry"
						country="c:B05"
						levels=2
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_muskets" }
			}
			create_building={
				building="building_doodad_manufacturies"
				add_ownership={
					building={
						type="building_doodad_manufacturies"
						country="c:B05"
						levels=1
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_crystal_socketry_building_doodad_manufacturies" }
			}
			create_building={
				building="building_military_shipyards"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden"  }
			}
			create_building={
				building="building_university"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_scholastic_education" "pm_secular_academia" }
			}
			
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:B05"
						levels=2
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_tools_building_rice_farm" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:B05"
						levels=1
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
			create_building={
				building="building_coffee_plantation"
				add_ownership={
					building={
						type="building_coffee_plantation"
						country="c:B05"
						levels=1
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_coal_mine"
				add_ownership={
					building={
						type="building_coal_mine"
						country="c:B05"
						levels=2
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:B05"
						levels=2
						region="STATE_STEEL_BAY"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_GREENHILL = {
		region_state:B05 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:B05"
						levels=1
						region="STATE_GREENHILL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_tools_building_rice_farm" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B05"
						levels=2
						region="STATE_GREENHILL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_livestock_ranch"
						country="c:B05"
						levels=1
						region="STATE_GREENHILL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:B05"
						levels=1
						region="STATE_GREENHILL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			
		}
	}

	s:STATE_RYAIL = {
		region_state:B05 = {
			create_building={
				building="building_tobacco_plantation"
				add_ownership={
					building={
						type="building_tobacco_plantation"
						country="c:B05"
						levels=1
						region="STATE_RYAIL"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_rice_farm"
				add_ownership={
					building={
						type="building_rice_farm"
						country="c:B05"
						levels=1
						region="STATE_RYAIL"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_tools_disabled" "pm_fig_orchards_building_rice_farm" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_SCATTERED_ISLANDS = {
		region_state:B05 = {
			create_building={
				building="building_banana_plantation"
				add_ownership={
					building={
						type="building_banana_plantation"
						country="c:B05"
						levels=1
						region="STATE_SCATTERED_ISLANDS"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_BRISTAILEAN = {
		region_state:A08 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A08"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B05 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_GUARDIAN_ISLANDS = {
		region_state:B07 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B07"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B08 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B08"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_OZTENCOST = {
		region_state:B03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B04 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B04"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B05 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_MINAR_YOLLI = {
		region_state:A03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
		region_state:B09 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B09"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

}