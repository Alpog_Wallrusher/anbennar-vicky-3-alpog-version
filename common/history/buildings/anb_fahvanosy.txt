﻿BUILDINGS = {
	s:STATE_BAVABETSARO = {
		region_state:B03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_AMEZATANY = { #3 mil base? theres a lot of halflings
		region_state:L35 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L35"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:A03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:A09 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A09"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:L46 = {
		create_building={
			building="building_port"
			add_ownership={
				country={
					country="c:L46"
					levels=1
				}
			}
			reserves=1
			activate_production_methods={ "pm_basic_port" }
		}
	}
}

	s:STATE_MIHITARAB = {	
		region_state:L47 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L47"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:A09 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A09"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_MAPOTIBOKBO = {
		region_state:L47 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L47"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:L49 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L49"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_IBIDINTELY = {
		region_state:L47 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L47"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:L48 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L48"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		
		}
		region_state:A09 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A09"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:L50 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L50"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_MAZAVA = {
		region_state:L51 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L51"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:L20 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L20"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:A03 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A03"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
		region_state:A04 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:A04"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			
		}
	}

	s:STATE_SIL_BERKHENS_LAND = {
		region_state:L52 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:L52"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

}