﻿BUILDINGS={
	s:STATE_MURDKATHER = {
		region_state:B85 = {
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:B85"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_maize_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B85"
						levels=1
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" }
			}
			create_building={
				building="building_vineyard_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B85"
						levels=1
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:B85"
						levels=1
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_softwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B85"
						levels=3
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
			create_building={
				building="building_glassworks"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:B85"
						levels=1
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_disabled_ceramics" "pm_manual_glassblowing" }
			}
			create_building={
				building="building_shipyards"
				add_ownership={
					building={
						type="building_shipyards"
						country="c:B85"
						levels=1
						region="STATE_MURDKATHER"
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding"  }
			}
			create_building={
				building="building_military_shipyards"
				add_ownership={
					country={
						country="c:B85"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B85"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_DEARKTIR = {
		region_state:B85 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B85"
						levels=3
						region="STATE_DEARKTIR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B85"
						levels=2
						region="STATE_DEARKTIR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
			create_building={
				building="building_tooling_workshops"
				add_ownership={
					building={
						type="building_tooling_workshops"
						country="c:B85"
						levels=1
						region="STATE_DEARKTIR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_tools" "pm_automation_disabled" }
			}
		}
	}

	s:STATE_PASKALA = {
		region_state:B85 = {
			create_building={
				building="building_maize_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B85"
						levels=1
						region="STATE_PASKALA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B85"
						levels=2
						region="STATE_PASKALA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B85"
						levels=2
						region="STATE_PASKALA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:B85"
						levels=1
						region="STATE_PASKALA"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
		}
		region_state:B05 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B05"
						levels=1
						region="STATE_PASKALA"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_TRIMTHIR = {
		region_state:B85 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B85"
						levels=1
						region="STATE_TRIMTHIR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B85"
						levels=1
						region="STATE_TRIMTHIR"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
		}
	}

	s:STATE_FOGRACLEAK = {
		region_state:B05 = {
			create_building={
				building="building_coal_mine"
				add_ownership={
					building={
						type="building_coal_mine"
						country="c:B05"
						levels=1
						region="STATE_FOGRACLEAK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_sulfur_mine"
				add_ownership={
					building={
						type="building_sulfur_mine"
						country="c:B05"
						levels=1
						region="STATE_FOGRACLEAK"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_sulfur_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_PELODARD = {
		region_state:B84 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=1
						region="STATE_PELODARD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_maize_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=2
						region="STATE_PELODARD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_GATHGOB = {
		region_state:B84 = {
			create_building={
				building="building_maize_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=1
						region="STATE_GATHGOB"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B84"
						levels=2
						region="STATE_GATHGOB"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B84"
						levels=1
						region="STATE_GATHGOB"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:B84"
						levels=1
						region="STATE_GATHGOB"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
		}
		region_state:B05 = {
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B05"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B05"
						levels=1
						region="STATE_GATHGOB"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_TRASAND = {
		region_state:B84 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B84"
						levels=2
						region="STATE_TRASAND"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_lead_mine"
				add_ownership={
					building={
						type="building_lead_mine"
						country="c:B84"
						levels=1
						region="STATE_TRASAND"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_lead_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

	s:STATE_DARHAN = {
		region_state:B84 = {
			create_building={
				building="building_cotton_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=1
						region="STATE_DARHAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_maize_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=2
						region="STATE_DARHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" }
			}
			create_building={
				building="building_silk_plantation"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B84"
						levels=2
						region="STATE_DARHAN"
					}
				}
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_no_enhancements" "pm_road_carts"  }
			}
			create_building={
				building="building_damestear_mine"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:B84"
						levels=1
						region="STATE_DARHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_damestear_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_financial_district"
						country="c:B84"
						levels=2
						region="STATE_DARHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_traditional_looms" "pm_craftsman_sewing" }
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:B84"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_construction_sector"
				add_ownership={
					country={
						country="c:B84"
						levels=2
					}
				}
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}

	s:STATE_EINNSAG = {
		region_state:B82 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B82"
						levels=1
						region="STATE_EINNSAG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_furniture_manufacturies"
				add_ownership={
					building={
						type="building_furniture_manufacturies"
						country="c:B82"
						levels=1
						region="STATE_EINNSAG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_luxury_furniture" "pm_automation_disabled" }
			}
			create_building={
				building="building_whaling_station"
				add_ownership={
					building={
						type="building_whaling_station"
						country="c:B82"
						levels=1
						region="STATE_EINNSAG"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_whaling" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_RAITHLOS = {
		region_state:B82 = {
			create_building = {
				building = "building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B82"
						levels=1
						region="STATE_RAITHLOS"
					}
				}
				reserves = 1
				activate_production_methods= { "pm_simple_farming" } 
			}
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B82"
						levels=1
						region="STATE_RAITHLOS"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_SGLOLAD = {
		region_state:B83 = {
			create_building={
				building="building_whaling_station"
				add_ownership={
					building={
						type="building_whaling_station"
						country="c:B83"
						levels=1
						region="STATE_SGLOLAD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_whaling" "pm_unrefrigerated" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B83"
						levels=1
						region="STATE_SGLOLAD"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" }
			}
		}
	}

	s:STATE_FOGHARBAC = {
		region_state:B81 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B81"
						levels=1
						region="STATE_FOGHARBAC"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
		}
	}

	s:STATE_GALBHAN = {
		region_state:B80 = {
			create_building={
				building="building_livestock_ranch"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B80"
						levels=2
						region="STATE_GALBHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B80"
						levels=2
						region="STATE_GALBHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_textile_mills"
				add_ownership={
					building={
						type="building_textile_mills"
						country="c:B80"
						levels=1
						region="STATE_GALBHAN"
					}
				}
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_traditional_looms" "pm_no_luxury_clothes" }
			}
		}
	}

	s:STATE_JHORGASHIRR = {
		region_state:B80 = {
			create_building = {
				building = "building_wheat_farm"
				add_ownership={
					building={
						type="building_manor_house"
						country="c:B80"
						levels=1
						region="STATE_JHORGASHIRR"
					}
				}
				reserves = 1
				activate_production_methods= { "pm_simple_farming" } 
			}
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B80"
						levels=2
						region="STATE_JHORGASHIRR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_softwood" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B80"
						levels=1
						region="STATE_JHORGASHIRR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_paper_mills"
				add_ownership={
					building={
						type="building_paper_mills"
						country="c:B80"
						levels=1
						region="STATE_JHORGASHIRR"
					}
				}
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" }
			}
			create_building = {
				building = "building_food_industry"
				add_ownership={
					building={
						type="building_food_industry"
						country="c:B80"
						levels=1
						region="STATE_JHORGASHIRR"
					}
				}
				reserves = 1
				activate_production_methods= { "pm_bakery" "pm_disabled_canning" "pm_disabled_distillery" "pm_manual_dough_processing" } 
			}
			create_building={
				building="building_government_administration"
				add_ownership={
					country={
						country="c:B80"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" "pm_secular_bureaucrats" }
			}
		}
	}

	s:STATE_GEMRADCURT = {
		region_state:B80 = {
			create_building={
				building="building_fishing_wharf"
				add_ownership={
					building={
						type="building_fishing_wharf"
						country="c:B80"
						levels=2
						region="STATE_GEMRADCURT"
					}
				}
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" }
			}
			create_building={
				building="building_port"
				add_ownership={
					country={
						country="c:B80"
						levels=1
					}
				}
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
		}
	}

	s:STATE_ARMONADH = {
		region_state:B80 = {
			create_building={
				building="building_logging_camp"
				add_ownership={
					building={
						type="building_logging_camp"
						country="c:B80"
						levels=1
						region="STATE_ARMONADH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_no_equipment" "pm_road_carts" }
			}
			create_building={
				building="building_iron_mine"
				add_ownership={
					building={
						type="building_iron_mine"
						country="c:B80"
						levels=1
						region="STATE_ARMONADH"
					}
				}
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" }
			}
		}
	}

}