﻿COUNTRIES = {
	c:R03 ?= {
		effect_starting_technology_tier_4_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production
	}
}