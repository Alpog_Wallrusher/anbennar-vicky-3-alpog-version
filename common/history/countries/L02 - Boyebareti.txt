﻿COUNTRIES = {
	c:L02 ?= {
		effect_starting_technology_tier_5_tech = yes
		
		effect_starting_politics_traditional = yes

		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy	#slaver council
		activate_law = law_type:law_racial_segregation	#to enable mengi cultures to be accepted
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_laissez_faire # INVALID - Invalid with land based taxation
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_local_police	#slavecatcher authority
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_outlawed_dissent
		activate_law = law_type:law_tenant_farmers
		
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slave_trade	#maybe turn to legacy. ofehibi lands is still slave state

		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_only
		
	}
}