﻿COUNTRIES = {
	c:A01 ?= {
		effect_starting_technology_tier_2_tech = yes
		effect_starting_artificery_tier_2_tech = yes
		
		add_taxed_goods = g:grain

		#Blackpowder Anbennar setup
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia	#anbennar's first task is to get this reform going
		activate_law = law_type:law_national_guard

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_free_trade	#enforced by powers so Anbennar keeps open
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_dedicated_police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_tenant_farmers

		activate_law = law_type:law_censorship	#maybe to control press to protect the republic (autocratic feel)
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_artifice_encouraged
		
		# Monarchy Anbennar setup
		# activate_law = law_type:law_monarchy
		# activate_law = law_type:law_wealth_voting	#compromise by baronites and conservatives
		# activate_law = law_type:law_cultural_exclusion
		# activate_law = law_type:law_freedom_of_conscience
		# activate_law = law_type:law_elected_bureaucrats
		# activate_law = law_type:law_national_militia	#anbennar's first task is to get this reform going
		# activate_law = law_type:law_national_guard

		# activate_law = law_type:law_interventionism
		# activate_law = law_type:law_free_trade	#enforced by powers so Anbennar keeps open
		# activate_law = law_type:law_per_capita_based_taxation
		# activate_law = law_type:law_no_colonial_affairs
		# activate_law = law_type:law_local_police
		# activate_law = law_type:law_no_schools
		# activate_law = law_type:law_charitable_health_system
		# activate_law = law_type:law_tenant_farmers

		# activate_law = law_type:law_censorship
		# #activate_law = law_type:law_serfdom_banned
		# activate_law = law_type:law_child_labor_allowed
		# activate_law = law_type:law_women_own_property
		# activate_law = law_type:law_slavery_banned
		
		# activate_law = law_type:law_all_races_allowed
		# activate_law = law_type:law_dark_arts_banned
		# activate_law = law_type:law_amoral_artifice_banned
		# activate_law = law_type:law_artifice_encouraged

		# #All conspirators of first BPC
		# ig:ig_intelligentsia = {
		# 	add_ruling_interest_group = yes
		# }
		# ig:ig_devout = {
		# 	add_ruling_interest_group = yes
		# }
		# ig:ig_landowners = {
		# 	add_ruling_interest_group = yes
		# }
		# ig:ig_armed_forces = {
		# 	add_ruling_interest_group = yes
		# }

		# Clear all but costine
		remove_primary_culture = cu:west_damerian
		remove_primary_culture = cu:east_damerian
		remove_primary_culture = cu:pearlsedger
		remove_primary_culture = cu:esmari
		remove_primary_culture = cu:vernman
		remove_primary_culture = cu:crownsman
		remove_primary_culture = cu:silver_dwarf
		remove_primary_culture = cu:arbarani
		remove_primary_culture = cu:arannese
		remove_primary_culture = cu:wexonard
		remove_primary_culture = cu:exwesser	
		remove_primary_culture = cu:dovesworn_gnoll
		remove_primary_culture = cu:imperial_gnome
	}
}