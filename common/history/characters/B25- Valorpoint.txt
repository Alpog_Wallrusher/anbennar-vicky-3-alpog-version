﻿CHARACTERS = {
	c:B25 ?= {
		create_character = {
			first_name = "Cristof"
			last_name = "Bayman"
			historical = yes
			ruler = yes
			age = 49
			is_general = yes
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_pacifist
			traits = {
				shellshocked basic_artillery_commander
			}
		}
	}
}
