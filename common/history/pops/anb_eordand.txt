﻿POPS = {
	s:STATE_KARNEL = {
		region_state:B60 = {
			create_pop = {
				culture = snecboth
				size = 8190
			}
			create_pop = {
				culture = glacier_gnome
				size = 4810
			}
		}
	}
	s:STATE_ARMONADH = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 102000
			}
		}
	}
	s:STATE_ANHOLTIR = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 78000
			}
		}
		region_state:B60 = {
			create_pop = {
				culture = glacier_gnome
				size = 3020
			}
			create_pop = {
				culture = ice_sleeper
				size = 5700
			}
		}
		region_state:B62 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 5320
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 300
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 13020
			}
		}
	}
	s:STATE_GALBHAN = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 212300
			}
			create_pop = {
				culture = tuathak
				size = 122400
			}
		}
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 5300
			}
		}
	}
	s:STATE_JHORGASHIRR = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 316100
			}
			create_pop = {
				culture = tuathak
				size = 191400
			}
			create_pop = {
				culture = peitar
				size = 52200
			}
			create_pop = {
				culture = cliff_gnome
				size = 5800
			}
			create_pop = {
				culture = dalr
				size = 14500
			}
		}
	}
	s:STATE_GEMRADCURT = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 409755
			}
			create_pop = {
				culture = peitar
				size = 50930
			}
			create_pop = {
				culture = cliff_gnome
				size = 2315
			}
		}
	}
	s:STATE_DARTIR = {
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 290000
			}
		}
	}
	s:STATE_FOGHARBAC = {
		region_state:B81 = {
			create_pop = {
				culture = snecboth
				size = 130000
			}
			create_pop = {
				culture = tuathak
				size = 343200
			}
			create_pop = {
				culture = peitar
				size = 46800
			}
		}
	}
	s:STATE_RAITHLOS = {
		region_state:B82 = {
			create_pop = {
				culture = snecboth
				size = 73200
			}
			create_pop = {
				culture = tuathak
				size = 433100
			}
			create_pop = {
				culture = peitar
				size = 103700
			}
		}
	}
	s:STATE_EINNSAG = {
		region_state:B82 = {
			create_pop = {
				culture = tuathak
				size = 594040
			}
			create_pop = {
				culture = peitar
				size = 50960
			}
		}
	}
	s:STATE_SGLOLAD = {
		region_state:B83 = {
			create_pop = {
				culture = tuathak
				size = 471000
			}
		}
	}
	s:STATE_TRASAND = {
		region_state:B84 = {
			create_pop = {
				culture = selphereg
				size = 448400
			}
			create_pop = {
				culture = peitar
				size = 141600
			}
		}
	}
	s:STATE_DARHAN = {
		region_state:B84 = {
			create_pop = {
				culture = selphereg
				size = 1280400
			}
			create_pop = {
				culture = peitar
				size = 39600
			}
		}
	}
	s:STATE_GATHGOB = {
		region_state:B84 = {
			create_pop = {
				culture = selphereg
				size = 809845
			}
		}
		region_state:B05 = {
			create_pop = {
				culture = selphereg
				size = 51455
			}
			create_pop = {
				culture = vanburian
				size = 8700
			}
		}
	}
	s:STATE_PELODARD = {
		region_state:B84 = {
			create_pop = {
				culture = selphereg
				size = 206700
			}
			create_pop = {
				culture = peitar
				size = 323300
			}
		}
	}
	s:STATE_PASKALA = {
		region_state:B85 = {
			create_pop = {
				culture = caamas
				size = 908622
			}
		}
		region_state:B05 = {
			create_pop = {
				culture = caamas
				size = 53258
			}
			create_pop = {
				culture = vanburian
				size = 12120
			}
		}
	}
	s:STATE_DEARKTIR = {
		region_state:B85 = {
			create_pop = {
				culture = caamas
				size = 508790
			}
			create_pop = {
				culture = peitar
				size = 10200
			}
		}
		region_state:B84 = {
			create_pop = {
				culture = peitar
				size = 94010
			}
		}
	}
	s:STATE_MURDKATHER = {
		region_state:B85 = {
			create_pop = {
				culture = caamas
				size = 1200300
			}
			create_pop = {
				culture = peitar
				size = 18900
			}
		}
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 37800
			}
			create_pop = {
				culture = caamas
				size = 3000
			}
		}
	}
	s:STATE_TRIMGARB = {
		region_state:B85 = {
			create_pop = {
				culture = peitar
				size = 323010
			}
		}
		region_state:B80 = {
			create_pop = {
				culture = snecboth
				size = 59930
			}
			create_pop = {
				culture = peitar
				size = 50400
			}
		}
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 27660
			}
		}
	}
	s:STATE_TRIMTHIR = {
		region_state:B85 = {
			create_pop = {
				culture = caamas
				size = 160800
			}
			create_pop = {
				culture = fograc
				size = 16100
			}
		}
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 63100
			}
		}
	}
	s:STATE_FOGRACLEAK = {
		region_state:B05 = {
			create_pop = {
				culture = fograc
				size = 201000
			}
			create_pop = {
				culture = caamas
				size = 39000
			}
			create_pop = {
				culture = vanburian
				size = 18000
			}
		}
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 42000
			}
		}
	}
	s:STATE_EGASACH = {
		region_state:B88 = {
			create_pop = {
				culture = fograc
				size = 88650
			}
			create_pop = {
				culture = caamas
				size = 1350
			}
		}
		region_state:B79 = {
			create_pop = {
				culture = fograc
				size = 12000
			}
			create_pop = {
				culture = haraf_ne
				size = 3000
			}
		}
	}
	s:STATE_DOMANDROD = {
		region_state:B86 = {
			create_pop = {
				culture = peitar
				size = 12000
			}
		}
	}
	s:STATE_SPRING_GLADE = {
		region_state:B86 = {
			create_pop = {
				culture = peitar
				size = 12000
			}
		}
	}
	s:STATE_SUMMER_GLADE = {
		region_state:B86 = {
			create_pop = {
				culture = peitar
				size = 12000
			}
		}
	}
	s:STATE_AUTUMN_GLADE = {
		region_state:B86 = {
			create_pop = {
				culture = peitar
				size = 12000
			}
		}
	}
	s:STATE_WINTER_GLADE = {
		region_state:B86 = {
			create_pop = {
				culture = peitar
				size = 12000
			}
		}
	}
}