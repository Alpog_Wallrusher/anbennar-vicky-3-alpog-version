﻿cannorian_religion = {
	heritage = yes
}

cannorian_religion_tolerance = {
	heritage = yes
}

sun_cult = {
	heritage = yes
}

faithless = {
	heritage = yes
}

kheteratan_religion = {
	heritage = yes
}

gnollish_religion = {
	heritage = yes
}

dwarven_religion = {
	heritage = yes
}

gerudian_religion = {
	heritage = yes
}

orcish_religion = {
	heritage = yes
}

dragon_cult = {
	heritage = yes
}

goblin_religion = {
	heritage = yes
}

taychendi_religion = {
	heritage = yes
}

ynnic_religion = {
	heritage = yes
}

fey = {
	heritage = yes
}

raheni_religion = {
	heritage = yes
}

halessi_religion = {
	heritage = yes
}

giantkin = {
	heritage = yes
}

centaur_religion = {
	heritage = yes
}

triunic_religion = {
	heritage = yes
}

elven_religion = {
	heritage = yes
}

harpy_religion = {
	heritage = yes
}

harafic_religion = {
	heritage = yes
}

eordan_religion = {
	heritage = yes
}

rzentur_religion = {
	heritage = yes
}

noruinic_religion = {
	heritage = yes
}

cursed_one_religion = {
	heritage = yes
}

etchings_of_the_deep_religion = {
	heritage = yes
}

leechfather_religion = {
	heritage = yes
}

soruinic_religion = {
	heritage = yes
}

effelai_religion = {
	heritage = yes
}

devandi_religion = {
	heritage = yes
}

mengi_religion = {
	heritage = yes
}

baashidi_religion = {
	heritage = yes
}

eldritch_cult = {
	heritage = yes
}

shadow_plane_religion = {
	heritage = yes
}

noukahi_religion = {
	heritage = yes
}

fangaulan_religion = {
	heritage = yes
}

lizardfolk_religion = {
	heritage = yes
}

tanizu_religion = {
	heritage = yes
}

fahvanosy_religion = {
	heritage = yes
}