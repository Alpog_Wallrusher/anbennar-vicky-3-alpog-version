﻿centaur_race_heritage = {
	heritage = yes
}

dwarven_race_heritage = {
	heritage = yes
}

elven_race_heritage = {
	heritage = yes
}

gnollish_race_heritage = {
	heritage = yes
}

gnomish_race_heritage = {
	heritage = yes
}

goblin_race_heritage = {
	heritage = yes
}

halfling_race_heritage = {
	heritage = yes
}

half_elven_race_heritage = {
	heritage = yes
}

half_orcish_race_heritage = {
	heritage = yes
}

harimari_race_heritage = {
	heritage = yes
}

harpy_race_heritage = {
	heritage = yes
}

hobgoblin_race_heritage = {
	heritage = yes
}

# Should be unused
human_race_heritage = {
	heritage = yes
}

kobold_race_heritage = {
	heritage = yes
}

lizardman_race_heritage = {
	heritage = yes
}

mechanim_race_heritage = {
	heritage = yes
}

ogre_race_heritage = {
	heritage = yes
}

orcish_race_heritage = {
	heritage = yes
}

# Should be unused
ruinborn_race_heritage = {
	heritage = yes
}

troll_race_heritage = {
	heritage = yes
}

fungoid_race_heritage = {
	heritage = yes
}

minotaur_race_heritage = {
	heritage = yes
}

mechanim_race_heritage = {
	heritage = yes
}
