﻿### ERA 1

damestear_meteorology = {
	era = era_1
	texture = "gfx/interface/icons/invention_icons/sericulture.dds"
	category = production
	can_research = yes
	
	modifier = {
		#country_resource_discovery_chance_mult = 100
	}
	
	ai_weight = {
		value = 1
	}
}

punch_card_artificery = {
	# Unlocks Artificeries building 
	# Unlocks Magical Flow Nexus building
	# Unlocks Lightning-Imbued Nets PM for Fishing Wharf
	# Unlocks Lightning-Imbued Harpoons PM for Whaling Station
	era = era_2	#so its not auto unlocked by most
	category = production
	can_research = yes
	
	unlocking_technologies = {
		lathe	#you gotta be able to do this to make punch cards!
	}

	ai_weight = {
		value = 1
	}
}

### ERA 2

elemental_elicitation = {
	# Unlocks Motor Industries building (it's different now!)
	# Unlocks Lightning-Infused Explosives PM for Explosive Plants
	era = era_2
	category = production
	can_research = yes
	
	unlocking_technologies = {
		punch_card_artificery
		#fractional_distillation
	}

	ai_weight = {
		value = 1
	}
}


# sparkdrives = {
# 	# Unlocks Motor Industries building (it's different now!)
# 	# Unlocks Lightning-Infused Explosives PM for Explosive Plants
# 	era = era_2
# 	category = production
# 	can_research = yes
	
# 	unlocking_technologies = {
# 		#punch_card_artificery
# 	}

# 	ai_weight = {
# 		value = 1
# 	}
# }

# chem_bound_magic = {
# 	# Unlocks Plant Growth in a Box PM for Chemical Plants 
# 	# Unlocks Growth Beans PM for Farms and Plantations
# 	era = era_2
# 	category = production
# 	can_research = yes
	
# 	unlocking_technologies = {
# 		#punch_card_artificery
# 		#intensive_agriculture
# 		fractional_distillation
# 	}

# 	ai_weight = {
# 		value = 1
# 	}
# }

thaumic_tearite = {
	# Unlocks Thaumic Steel Casings PM for Artificeries
	# Unlocks Mithril-Tempered Steel PM for Mithril Forges
	era = era_2
	category = production
	can_research = yes
	
	unlocking_technologies = {
		#sparkdrives
		elemental_elicitation
		bessemer_process
	}

	ai_weight = {
		value = 1
	}
}

insyaan_analytical_engine = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_2
	category = production
	can_research = yes
	
	unlocking_technologies = {
		punch_card_artificery
	}

	ai_weight = {
		value = 1
	}
}

brass_prosthesis = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_2
	category = production
	can_research = yes
	
	unlocking_technologies = {
		mechanical_tools
		insyaan_analytical_engine
	}

	modifier = {
		building_working_conditions_mult = -0.05
	}

	ai_weight = {
		value = 1
	}
}

### ERA 3

thinking_servos = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		brass_prosthesis
		magical_wiring
	}

	ai_weight = {
		value = 1
	}
}

# enchanting_for_efficiency = {
# 	# Unlocks Speed Enchantments PM in Motor Industries 
# 	# Unlocks Boxes of Holding PM in Railways
# 	# Unlocks Condensed Magical Hearts in Alchemist Labs
# 	era = era_3
# 	category = production
# 	can_research = yes
	
# 	unlocking_technologies = {
# 		#thaumic_tearite
# 		watertube_boiler
# 		#railways
# 	}

# 	modifier = {
# 		building_economy_of_scale_level_cap_add = 5
# 	}	

# 	ai_weight = {
# 		value = 1
# 	}
# }

magical_wiring = {
	# Unlocks Electrics Industries building (w/ sending stones)
	# Unlocks Auto-Patterning Needles PM for Textile Mills
	# Unlocks Arcane Copper "Arcop" Wires for Artificeries
	# Unlocks High Velocity Irrigation PMs for Plantations
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		thaumic_tearite
	}

	ai_weight = {
		value = 1
	}
}

magic_wave_stabilization = {
	# Unlocks Yana-Stabilized Thaumic Casings for Artificeries
	# Unlocks Magic Batteries for Magical Flow Nexus
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		thaumic_tearite
		chemical_bleaching
	}

	ai_weight = {
		value = 1
	}
}

spirit_capacitors = {
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		magic_wave_stabilization
	}

	ai_weight = {
		value = 1
	}
}

early_mechanim = {
	# Unlocks Mechanim Laborer Production for Automatories
	# Unlocks Remote-Pilotable Automata for Automatories
	# Unlocks Self-Building Automata for Automatories
	# Unlocks Automata-Stirred Potion Vats for Alchemist Labs
	# Unlocks Automata Dockworkers for Dockyards and Naval Dockyards
	# Unlocks Automata Craftsmen for Electrics Industries
	# Unlocks Harvester Automata for Plantations
	# Unlocks Automata Miners for Mines
	# Unlocks Automata-Operated Steam Donkeys
	# Unlocks Automata-Watched Boilers for (lots)
	# Unlocks Automata Laborers for (lots)
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		insyaan_analytical_engine
		mechanized_workshops
	}

	ai_weight = {
		value = 1
	}
}

pathway_transmutation = {
	# Unlocks Mineral Transmutation for Mines
	# Unlocks Transmutative Extraction Pulping for Paper Mills
	# Unlocks Polymorphed Megasheep for Livestock 
	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		#magic_wave_stabilization
		fractional_distillation
	}

	ai_weight = {
		value = 1
	}
}

gene_transmutation = {

	era = era_3
	category = production
	can_research = yes
	
	unlocking_technologies = {
		baking_powder	#purely because this provides an upgrade from Baking Powder PM in Food Industries
	}

	# disabling_technologies = { "lingering_verdancy" } this goes in the trait, not here (i moved it)

	ai_weight = {
		value = 1
	}
}

### ERA 4 

industrial_exosuits = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		thinking_servos
	}

	modifier = {
		building_working_conditions_mult = -0.1
	}	

	ai_weight = {
		value = 1
	}
}

soul_cage = {

	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		spirit_capacitors
	}

	ai_weight = {
		value = 1
	}
}

chronoculture = {
	# Unlocks Chronogrowth for Farms and Plantations
	# Unlocks Accelerated Fermentation Patent Stills for Food Industries
	# Unlocks Soils of Perpetual Growth for Chemical Plants
	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		spirit_capacitors
		gene_transmutation
	}

	ai_weight = {
		value = 1
	}
}

electroarcanism_theory = {
	# Unlocks Damesoil Suspension for Magical Flow Nexus
	# Unlocks Sparkdrive Plant for Power Plant
	# Unlocks Damesoil Tubes for Artificeries
	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		electrical_generation
		#magical_wiring
		magic_wave_stabilization
	}

	ai_weight = {
		value = 1
	}
}

multimaterial_imbuement = {
	# Unlocks Unseen-Servant Furniture for Furniture Factories
	# Unlocks Ever-Changing Elastics for Textile Mills
	# Unlocks Self-Cleaning Yana for Glassworks
	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		vulcanization
		pathway_transmutation
	}

	ai_weight = {
		value = 1
	}
}

essence_extraction = {

	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		pumpjacks
		pathway_transmutation
	}

	ai_weight = {
		value = 1
	}
}

# arcano_synthesis = {

# 	era = era_4
# 	category = production
# 	can_research = yes
	
# 	unlocking_technologies = {
# 		essence_extraction
# 	}

# 	ai_weight = {
# 		value = 1
# 	}
# }

# terraformestry = {
# 	# Unlocks Old Growth Regeneration Sawmills for Logging Camps
# 	# Unlocks Full-Forest Hardwood Transformations for Logging Camps
# 	era = era_4
# 	category = production
# 	can_research = yes
	
# 	unlocking_technologies = {
# 		pathway_transmutation
# 		chronoculture
# 	}

# 	ai_weight = {
# 		value = 1
# 	}
# }

advanced_mechanim = {
	# Unlocks Automata-Managed Wells in Oil Rigs
	# Unlocks Automata-Managed Rails and Donkeys in Mines
	# Unlocks Automata-Managed Construction in Construction
	# Unlocks Automata-Managed Rotary Engines in (lots)
	# Unlocks Programmable Mechanim Production in Automatories
	# Unlocks Self-Managed Self-Building Automata in Automatories
	# Unlocks Automata-Managed Plantations in Plantations
	# Unlocks Automata Machinists in (lots)
	# Unlocks Automata Shipwrights in Shipyards
	# Unlocks Automata Technicians in Electrics Industries
	# Unlocks Automata Conductors in Railways
	era = era_4
	category = production
	can_research = yes
	
	unlocking_technologies = {
		early_mechanim
		#multimaterial_imbuement
		magical_wiring
	}

	ai_weight = {
		value = 1
	}
}


### ERA 5 

apparitional_communicators = {
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		radio
		electroarcanism_theory
	}

	ai_weight = {
		value = 1
	}
}

anti_friction_magnemancy = {
	# Unlocks Anti-Friction Clamps for Railways
	# Unlocks Damesoil Magnetism for Oil Rigs
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		electric_railway
		electroarcanism_theory
	}

	ai_weight = {
		value = 1
	}
}

artificial_life = {
	# Unlocks Synthetic Monster Parts in Alchemist Labs
	# Unlocks Damesoil Liquefaction in Alchemist Labs
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		#gene_transmutation
		chronoculture
		pasteurization	#to make it more of an endgame thing PLUS this requires electricity aka frankenstein vibes
	}

	ai_weight = {
		value = 1
	}
}

morphic_transmutation = {
	# Unlocks Domesticated Wild Magics in Explosive Plants
	# Unlocks Target-Selecting Explosions in Mines
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		essence_extraction
		multimaterial_imbuement
		compression_ignition
		plastics
	}

	ai_weight = {
		value = 1
	}
}

the_fifth_fundamental_force = {
	# Unlocks Damestear Reactor in Electric Plant
	# Unlocks Damestear Core in Artificeries
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		#electroarcanism_theory
		anti_friction_magnemancy
		oil_turbine
	}

	ai_weight = {
		value = 1
	}
}

mimic_precursor_steel = {
	# Unlocks Mimic Precursor Steel in Artificeries
	# Unlocks Mimic Precursor Steel Buildings in Construction
	# Unlocks Damestear-Tempered Mithril in Mithril Forges
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		arc_welding
		multimaterial_imbuement
	}

	ai_weight = {
		value = 1
	}
}

mechs = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		industrial_exosuits
		mechanized_farming
	}

	ai_weight = {
		value = 1
	}
}

subterrenes = {
	# Unlocks Automatories building
	# Unlocks Automata Laborers PM in various places
	# Unlocks Cannon Autoloader Golems PMs in Automatories
	era = era_5
	category = production
	can_research = yes
	
	unlocking_technologies = {
		mechs
		arc_welding
	}

	ai_weight = {
		value = 1
	}
}