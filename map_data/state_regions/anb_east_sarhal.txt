﻿STATE_MENGABOYS_1 = {
    id = 800
    subsistence_building = "building_subsistence_farms"
    provinces = { "x6E54CC" "x6FCE70" "x785BF1" "xACA054" "xFF6C4A" }
    traits = {}
    city = "xff6c4a" #Yemetsimira
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "xaca054" #Lipmekat
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3401
}
STATE_MENGABOYS_2 = {
    id = 801
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0676C1" "x584FA1" "x6E35D5" "x7F0A83" "x88FB53" "xA5BFE0" "xBBD931" "xBCA657" "xDC7E80" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "xbca657" #Neekshaff
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3401
}
STATE_MENGABOYS_3 = {
    id = 802
    subsistence_building = "building_subsistence_farms"
    provinces = { "x540C7D" "x61D33E" "x84D7E4" "xF1D14D" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4
        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_4 = {
    id = 803
    subsistence_building = "building_subsistence_farms"
    provinces = { "x13646E" "x622856" "xAA77A5" "xDF9B79" "xFB1332" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4
        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_5 = {
    id = 804
    subsistence_building = "building_subsistence_farms"
    provinces = { "x403E5E" "x5FD4D5" "xAD36BE" "xEB5927" "xFBF5D9" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_6 = {
    id = 805
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A82FA" "x1F0E9A" "x22F1E8" "x8C568A" "xA7CBD3" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_7 = {
    id = 806
    subsistence_building = "building_subsistence_farms"
    provinces = { "x29A665" "xA22D1A" "xEC93BF" "xF2D82F" "xFECD1B" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_8 = {
    id = 807
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01B257" "x0A6F45" "x734BD5" "x860DB0" "xFC2598" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_9 = {
    id = 808
    subsistence_building = "building_subsistence_farms"
    provinces = { "x04685C" "x0549B4" "x37B05D" "x6DB719" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_10 = {
    id = 809
    subsistence_building = "building_subsistence_farms"
    provinces = { "x59FB7C" "x5FCE17" "xD69592" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_11 = {
    id = 810
    subsistence_building = "building_subsistence_farms"
    provinces = { "x37F3F7" "x449A3F" "x5033FD" "xC3551E" "xE89730" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_12 = {
    id = 811
    subsistence_building = "building_subsistence_farms"
    provinces = { "x402D48" "x4CB0E9" "x729E3A" "x961679" "xB77C6C" "xD129E0" "xE5B366" "xE7621C" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_13 = {
    id = 812
    subsistence_building = "building_subsistence_farms"
    provinces = { "x29F897" "x4AE859" "x50E2C4" "x784853" "x847F34" "xB56832" "xDB6A3E" "xF28AEF" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_14 = {
    id = 813
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D8A44" "x23F3F8" "x3A6103" "x5C452C" "x7E3A04" "xC5C652" "xDBA322" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_15 = {
    id = 814
    subsistence_building = "building_subsistence_farms"
    provinces = { "x07A3E9" "x454B41" "xA0F05E" "xCC340B" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_16 = {
    id = 815
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0FA71C" "x293DEC" "x67077B" "x830696" "xA491D1" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_17 = {
    id = 816
    subsistence_building = "building_subsistence_farms"
    provinces = { "x28E5B2" "x2B095D" "x4914EB" "x6FFF71" "x9C379A" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_18 = {
    id = 817
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1A5F2C" "x8ABD44" "x99BA2C" "xA37227" "xA41079" "xAFC9C2" "xEBCBB4" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_19 = {
    id = 818
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2CB55D" "x4133AF" "x838734" "xD90AD8" "xE08608" "xE616E0" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_20 = {
    id = 819
    subsistence_building = "building_subsistence_farms"
    provinces = { "x202FE4" "x38570E" "x3C816A" "x40A58B" "x4D7623" "x65580B" "xAE0510" "xB40676" "xCB0905" "xD656F8" "xF8B528" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "xb40676" #Irkan Eparta
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3301
}
STATE_MENGABOYS_21 = {
    id = 820
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1C566B" "x4C1D7C" "x551EE0" "x58630B" "x69DF43" "x99BA96" "xB25239" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_22 = {
    id = 821
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0370B1" "x068294" "x4007CF" "x47C2B5" "x785838" "x863213" "x9E56D5" "xA5245A" "xD2C2A2" }
    traits = {}
    city = "x863213" #Riyiwedeb
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "x9e56d5" #Tnismot
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3300
}
STATE_MENGABOYS_23 = {
    id = 822
    subsistence_building = "building_subsistence_farms"
    provinces = { "x21C393" "x74C950" "xC68C55" "xEF8527" "xF5777A" }
    traits = {}
    city = "xef8527" #Dakmender
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "x21c393" #Biribitid
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3300
}
STATE_BERI_GNIDI = {
    id = 823
    subsistence_building = "building_subsistence_farms"
    provinces = { "x047343" "x336593" "x901BFA" "xC3E1AB" "xE15E0E" "xEF0704" "xF6DF9F" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "x336593" #Bakrast Khaj - slaver port btw
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3300
}
STATE_DASMATUS = {
    id = 824
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0D06A1" "x2E13B5" "x458F49" "x64449C" "xA314A1" "xA7E304" "xD469F4" }
    traits = {}
    city = "x458f49" #Coburakaz or whatever, aka Azka-szel-Udam
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    port = "x0d06a1" #Dasmatus - actual Dasmatus itself
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
    naval_exit_id = 3300
}
STATE_GREATER_GIZAN = {
    id = 825
    subsistence_building = "building_subsistence_farms"
    provinces = { "x27A59F" "x2D1419" "x3A74C4" "x829553" "x91BD0D" "xE58E1A" "xEFAE44" "xFDF3E1" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_HARAAGTSEDA = {
    id = 826
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0C0E29" "x2ED580" "x308BAF" "x5255C7" "x6A0A8C" "x725A54" "x875830" "x9657A9" "x9A1713" "xA55F32" "xA87CD2" "xBB46E7" "xC2C916" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_KOGZALLA = {
    id = 827
    subsistence_building = "building_subsistence_farms"
    provinces = { "x112BCE" "x6AB135" "x6F9647" "xA466F0" "xA87D9D" "xC99ABA" "xFE8F9F" }
    impassable = { "x112bce" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_29 = {
    id = 828
    subsistence_building = "building_subsistence_farms"
    provinces = { "x06AC72" "x083CFD" "x110642" "x164EB0" "x59EB87" "x6D1E24" "x87E151" "xB0244A" "xCEF8E8" "xD25C03" "xDCCFEA" "xE185B2" "xF3DF24" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_30 = {
    id = 829
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0102FA" "x0142A9" "x07C9FE" "x1BF2FF" "x57AF64" "x604664" "x64400F" "x9BDB12" "xA3A6D5" "xC73232" "xF1FBE7" "xFF92AD" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_31 = {
    id = 830
    subsistence_building = "building_subsistence_farms"
    provinces = { "x13C840" "x185E4B" "x1ECDB6" "x2ACCB9" "x65CA96" "x99C3CB" "xC00C25" "xCE4592" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_32 = {
    id = 831
    subsistence_building = "building_subsistence_farms"
    provinces = { "x055B06" "x074C4D" "x31600A" "x3602CC" "x452CD6" "x55D154" "x634B14" "xA22D60" "xA45E7E" "xCD0740" "xCD69F5" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
STATE_MENGABOYS_33 = {
    id = 832
    subsistence_building = "building_subsistence_farms"
    provinces = { "x12D63F" "x175AFE" "x1CC813" "x39F374" "x813426" "x854A53" "x9D504B" "xB8F070" "xD20581" "xF5B47D" "xFDB661" }
    traits = {}
    city = "xD50177" #PLACEHOLDER REPLACE ME
    farm = "xD50177" #PLACEHOLDER REPLACE ME
    wood = "xD50177" #PLACEHOLDER REPLACE ME
    mine = "xD50177" #PLACEHOLDER REPLACE ME
    arable_land = 160
    arable_resources = { bg_millet_farms bg_livestock_ranches }
    capped_resources = {
        bg_iron_mining = 36
        bg_coal_mining = 92 #from eu4


        bg_logging = 9
        bg_fishing = 17
        bg_lead_mining = 48
        bg_damestear_mining = 10
    }
}
